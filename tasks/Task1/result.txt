Работа с файлом wiki.txt:

root@quickstart:/# time python3 job.py wiki.txt -o result_local

real    1m56.149s
user    1m51.220s
sys     0m2.072s


root@quickstart:/# time python3 job.py -r hadoop hdfs:///user/root/wiki.txt -o hdfs:///user/root/result_hadoop

real    1m21.645s
user    1m19.708s
sys     0m1.837s



Работа с файлом wiki_trunc.txt:

root@quickstart:/# time python3 job.py wiki_trunc.txt -o result_local_2

real	0m4,966s
user	0m4,884s
sys	    0m0,082s


root@quickstart:/# time python3 job.py -r hadoop hdfs:///user/root/wiki_trunc.txt -o hdfs:///user/root/result_hadoop_2

real	0m30,142s
user	0m29,717s
sys	    0m0,584s


Из результатов временных затрат можно сделать вывод, что использование hadoop имеет смысл на файлах большого размера,
т.к. прежде чем преступить к выполнению вычислений, происходит продолжительная подготовительная работа hadoop. В связи с
этим очень заметна большие затраты времени при работе с маленькими файлами.