from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[A-Za-zА-Яа-я]+\.[A-Za-zА-Яа-я]+\.")

threshold_count = 20

class MRAbbreviations_2(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            yield word.lower(), 1

    def combiner(self, word, counts):
        yield word, sum(counts)

    def reducer(self, word, counts):
        count = sum(counts)
        if count > threshold_count:
            yield word, str(count)


if __name__ == '__main__':
    MRAbbreviations_2.run()