from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[A-Za-z]+")


class MRFreqLatinWord(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            yield word.lower(), 1

    def combiner(self, word, counts):
        yield None, (sum(counts), word)

    def reducer(self, word, counts):
        yield 'The most common latin word:', max(counts)


if __name__ == '__main__':
    MRFreqLatinWord.run()