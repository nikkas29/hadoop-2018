from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[A-Za-zА-Яа-я]+[.]*")

threshold = 0.9
threshold_count = 30

class MRAbbreviations(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            word_without_dots = word.lower().replace(".", "")
            if len(word_without_dots) > 1:
                yield word_without_dots, word[-1] == '.'

    def reducer(self, word, counts):
        lst = list(counts)
        words_all = len(lst)
        words_with_dots = lst.count(True)
        if words_all > threshold_count and words_with_dots > words_all * threshold:
            yield word + '.', (str(words_with_dots) + '/' + str(words_all))

if __name__ == '__main__':
    MRAbbreviations.run()