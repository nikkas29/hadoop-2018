from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[A-Za-zА-Яа-я]+")


class MRLongestWord(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            yield word.lower(), len(word)

    def combiner(self, word, lengths):
        yield None, (list(lengths)[0], word)

    def reducer(self, _, words):
        yield max(words)



if __name__ == '__main__':
    MRLongestWord.run()