from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[A-Za-zА-Яа-я]+")

threshold = 0.5
threshold_count = 10


class MRWordWithCapitalLetter(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            word_low = word.lower()
            yield word_low, word[0] != word_low[0]

    def reducer(self, word, counts):
        lst = list(counts)
        words_all = len(lst)
        words_with_capital = lst.count(True)
        if words_all > threshold_count and words_with_capital > words_all * threshold:
            yield word, (str(words_with_capital) + '/' + str(words_all))


if __name__ == '__main__':
    MRWordWithCapitalLetter.run()
