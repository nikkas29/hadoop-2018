from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re
import statistics

WORD_RE = re.compile(r"[A-Za-zА-Яа-я]+")


class MRAverageWordLength(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            yield None, len(word)

    def reducer(self, _, lengths):
        result = statistics.mean(lengths)
        yield "Average word length:", result

if __name__ == '__main__':
    MRAverageWordLength.run()