import os
import errno

def concat_result(pathDir, nameFile):

    lst_files = []

    for root, dirs, files in os.walk("./" + pathDir):
        for filename in files:
            lst_files.append(filename)

    result_list = []

    for name in lst_files:
        try:
            with open("./" + pathDir + name, 'r') as f:
                result_list += list(f)
        except IOError as exc:
            if exc.errno != errno.EISDIR:
                raise

    result_list.sort()
    concat = ''.join(result_list)
    with open(nameFile, 'w') as res_file:
        res_file.write(concat)


concat_result("task_result_1/", "result_1.txt")
concat_result("task_result_2/", "result_2.txt")
concat_result("task_result_3/", "result_3.txt")
concat_result("task_result_4/", "result_4.txt")
concat_result("task_result_5/", "result_5.txt")
concat_result("task_result_6/", "result_6.txt")
